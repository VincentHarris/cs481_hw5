﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms.Maps;

namespace CS481_HW5
{
    public class CustomPin : Pin
    {
        public string Id { get; set; }
        public string Type { get; set; }
        
    }
}
