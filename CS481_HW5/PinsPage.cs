﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;
using Xamarin.Forms.Maps;

namespace CS481_HW5
{
    //references https://docs.microsoft.com/en-us/xamarin/xamarin-forms/app-fundamentals/custom-renderer/map/customized-pin
    public class PinsPage : ContentPage
    {
        Map map;
        public PinsPage()
        {
            map = new Map
            {
                IsShowingUser = true,
                HeightRequest = 100,
                WidthRequest = 960,
                VerticalOptions = LayoutOptions.FillAndExpand
            };

            map.MoveToRegion(MapSpan.FromCenterAndRadius(
                new Position(33.525383, -117.153442), Distance.FromMiles(3)));
            var position = new Position(33.525383, -117.153442);
            var pin = new Pin
            {
                Type = PinType.Place,
                Position = position,
                Label = "Temecula Promenade",
                Address = "https://www.promenadetemecula.com/en.html"
            };

            map.Pins.Add(pin);
            //referenced from 
            var otherPins = new Button { Text = "show other pins" };
            otherPins.Clicked += (sender, e) => {
                map.Pins.Add(new Pin
                {
                    Position = new Position(33.574676, -117.200206),
                    Label = "A&A Games",
                    Address = "https://www.facebook.com/aagames14"
                });
                map.Pins.Add(new Pin
                {
                    Position = new Position(33.507020, -117.146789),
                    Label = "NakaMaya",
                    Address = "https://www.nakamayatemecula.com/"
                });
                map.Pins.Add(new Pin
                {
                    Position = new Position(33.528981, -117.173439),
                    Label = "Temecula MotorSports",
                    Address = "https://www.temeculamotorsports.com/"
                });
                map.MoveToRegion(MapSpan.FromCenterAndRadius(
                    new Position(33.525383, -117.153442), Distance.FromMiles(5.0)));

            };
            
            var buttons = new StackLayout
            {
                Orientation = StackOrientation.Horizontal,
                Children = {otherPins}
            };

            Content = new StackLayout
            {
                Spacing = 0,
                Children = {
                    map,
                    buttons
                }
            };
        }
    }
}
