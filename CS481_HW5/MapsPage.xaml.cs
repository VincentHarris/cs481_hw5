﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
//using Xamarin.Essentials;
using Xamarin.Forms.Maps;

namespace CS481_HW5
{
    //referenced from https://www.youtube.com/watch?v=Tp10mphOm1Y&t=1s
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MapsPage : ContentPage
    {
        public MapsPage()
        {
            InitializeComponent();
            //Adds items to the picker so it'll show up
           // MainPicker.Items.Add("Temecula Promenade Mall");
            //MainPicker.Items.Add("A&A Games");
            //MainPicker.Items.Add("NakaMaya");
            //MainPicker.Items.Add("Temecula MotorSports");

            //This goes to coordinates specified (Which happens to be the Temecula Promenade Mall)
            //And zooms out so you can see a 5 mile radius from that specific spot
            var map = new Xamarin.Forms.Maps.Map(MapSpan.FromCenterAndRadius(
               new Position(33.525383, -117.153442),
               Distance.FromMiles(5.0)))
            {
                IsShowingUser = true,
                VerticalOptions = LayoutOptions.FillAndExpand
            };

            //These are the postions of the pins and their coordinates on Maps
            var position1 = new Position(33.525383, -117.153442);
            var position2 = new Position(33.574676, -117.200206);
            var position3 = new Position(33.507020, -117.146789);
            var position4 = new Position(33.528981, -117.173439);

            //This gives you the details of each pin, with type, position, label and website address
            var pin1 = new Pin
            {
                Type = PinType.Place,
                Position = position1,
                Label = "Temecula Promenade Mall",
                Address = "https://www.promenadetemecula.com/en.html",
            };

            var pin2 = new Pin
            {
                Type = PinType.Place,
                Position = position2,
                Label = "A&A Games",
                Address = "https://www.facebook.com/aagames14"
            };

            var pin3 = new Pin
            {
                Type = PinType.Place,
                Position = position3,
                Label = "NakaMaya",
                Address = "https://www.nakamayatemecula.com/"
            };

            var pin4 = new Pin
            {
                Type = PinType.Place,
                Position = position4,
                Label = "Temecula MotorSports",
                Address = "https://www.temeculamotorsports.com/"
            };

            
            //This then adds the pins onto the map with all their respective details
            map.Pins.Add(pin1);
            map.Pins.Add(pin2);
            map.Pins.Add(pin3);
            map.Pins.Add(pin4);
            Content = map;


           
        }
        /*
        void OnSliderValueChanged(object sender, ValueChangedEventArgs e)
        {
            double zoomLevel = e.NewValue;
            double latlongDegrees = 360 / (Math.Pow(2, zoomLevel));
            if (map.VisibleRegion != null)
            {
                map.MoveToRegion(new MapSpan(map.VisibleRegion.Center, latlongDegrees, latlongDegrees));
            }
        }
        */

        /*
        void OnButtonClicked(object sender, EventArgs e)
        {
            Button button = sender as Button;
            switch (button.Text)
            {
                case "Street":
                    Map.MapType = MapType.Street;
                    break;
                case "Satellite":
                    Map.MapType = MapType.Satellite;
                    break;
            }
        }
        */

            //This should allow you to select a name of a pin, and displays either some info, or perhaps a button to
            //take you to their position on the map
        /*    
        private void MainPicker_OnSelectedIndexChanged(object sender, EventArgs e)
            {
                var name = MainPicker.Items[MainPicker.SelectedIndex];
                DisplayAlert(name, "Selected Value", "Ok");
            }

        */
        /*
         //This allowed you to click the button and then it;ll open up maps and go to the coordinates specified
         private async void ClickedOpenCoordsButton(object sender, EventArgs e)
        {
            if (!double.TryParse(EntryLatitude.Text, out double lat))
                return;
            if (!double.TryParse(EntryLongitude.Text, out double lng))
                return;

            await Xamarin.Essentials.Map.OpenAsync(lat, lng, new MapLaunchOptions
            {
                Name = EntryName.Text,
                NavigationMode = NavigationMode.None

            });
            }
        */

    }
}