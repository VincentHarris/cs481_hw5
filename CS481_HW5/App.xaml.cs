﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using System.Linq;
using System.Text;

namespace CS481_HW5
{
    public partial class App : Application
    {
        public static double ScreenHeight;
        public static double ScreenWidth;

        public App()
        {
            InitializeComponent();

           // MainPage = new MapsPage(); // shows my pins at once, but that's it
           // MainPage = new CS481_HW5.MainPage(); doesn't work
            //MainPage = new CS481_HW5.MapPage(); works, but shows a blank page
            
            MainPage = new TabbedPage //tabs work (but don't have labels), buttons work and shows pins
            {
                Children =
                {   new MapPage(),
                    new PinsPage(),
                    //new MapsPage(),
                    //new MapTypes(),
                }
            };
            
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }

}
