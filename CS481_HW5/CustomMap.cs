﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms.Maps;

namespace CS481_HW5
{
    public class CustomMap : MapPage
    {
        public List<CustomPin> CustomPins { get; set; }
        public object MapType { get; internal set; }
    }
}
