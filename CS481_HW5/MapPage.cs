﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using Xamarin.Forms;
using Xamarin.Forms.Maps;

namespace CS481_HW5
{   
    //references https://www.youtube.com/watch?v=Tp10mphOm1Y&t=1s

    public class MapPage : ContentPage
    {
        Map map;
        public MapPage()
        {
            map = new Map
            {
                HeightRequest = 100,
                WidthRequest = 960,
                VerticalOptions = LayoutOptions.FillAndExpand
            };

            //Moves to the Temecula Promenade Position
            double zoomLevel = 0.5;
            double latlongDegrees = 360 / (Math.Pow(2, zoomLevel));
            map.MoveToRegion(new MapSpan(new Position(33.525383, -117.153442), 33.525383, -117.153442));          

            var street = new Button { Text = "Street" };
            var satellite = new Button { Text = "Satellite" };
            street.Clicked += HandleClicked;
            satellite.Clicked += HandleClicked;

            var segments = new StackLayout
            {
                Spacing = 30,
                HorizontalOptions = LayoutOptions.CenterAndExpand,
                Orientation = StackOrientation.Horizontal,
                Children = { street,  satellite }
            };


            var stack = new StackLayout { Spacing = 0 };
            stack.Children.Add(map);
            stack.Children.Add(segments);
            Content = stack;

        }
        //referenced from https://stackoverflow.com/questions/44669945/xamarin-forms-custom-map-pin
        void HandleClicked(object sender, EventArgs e)
        {
            var b = sender as Button;
            switch (b.Text)
            {
                case "Street":
                    map.MapType = MapType.Street;
                    break;
                case "Satellite":
                    map.MapType = MapType.Satellite;
                    break;
            }
        }

        //referenced from https://forums.xamarin.com/discussion/22493/maps-visibleregion-bounds
        static void CalculateBoundingCoordinates(MapSpan region)
        {
            var center = region.Center;
            var halfheightDegrees = region.LatitudeDegrees / 2;
            var halfwidthDegrees = region.LongitudeDegrees / 2;

            var left = center.Longitude - halfwidthDegrees;
            var right = center.Longitude + halfwidthDegrees;
            var top = center.Latitude + halfheightDegrees;
            var bottom = center.Latitude - halfheightDegrees;

            if (left < -180) left = 180 + (180 + left);
            if (right > 180) right = (right - 180) - 180;

            /*
            private object customMap;

            public MapPage()
            {

            var CustomMap = new CustomMap
            {
                MapType = Maptype.Street,
                WidthRequest = App.ScreenWidth,
                HeightRequest = App.ScreenHeight
            };

            var pin = new CustomPin
            {
                Type = PinType.Place,
                Position = new Position(33.525383, -117.153442),
                Label = "Temecula Promenade Mall",
                Address = "40820 Winchester Rd, Temecula CA",
                Id = "Temecula Mall"
            };

                var pin = new CustomPin
                {
                    Type = PinType.Place,
                    Position = new Position(33.574676, -117.200206),
                    Label = "A&A Games",
                    Address = "40635 California Oaks, Murrieta CA",
                    Id = "A&A"
                };

                var pin = new CustomPin
                {
                    Type = PinType.Place,
                    Position = new Position(33.507020, -117.146789),
                    Label = "NakaMaya",
                    Address = "27513 Ynez Rd, Temecula CA",
                    Id = "Naka"
                };

                customMap.CustomPins = new List<CustomPin> {pin};
                customMap.Pins.Add(pin);
                customMap.Pins.Add(pin);
                customMap.MoveToRegion(MapSpan,FromCenterAndRadius(new Position(33.525383, -117.153442), Distance.FromMiles(1.0)));
                Content = customMap;
            }
         */
        }
    }
}